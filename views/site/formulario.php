<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario $model */
/** @var ActiveForm $form */
?>
<div class="site-formulario">

    <?php $form = ActiveForm::begin(); ?>

        <?= 
            $form
                ->field($model, 'nombre') 
                ->textInput([
                    "placeholder" => "introduce nombre completo"
                ])
        ?>
        <?= 
            $form
                ->field($model, 'apellidos') 
                ->textInput([
                    "placeholder" => "Introduce dos apellidos"
                ])
        ?>
        <?= 
            $form
                ->field($model, 'fechaNacimiento') 
                ->input("date")
        ?>
        <?= 
            $form
                ->field($model, 'correo') 
                ->input("email")
        ?>
        <?= 
            $form
                ->field($model, 'poblacion') 
                ->dropDownList(
                    $model->poblaciones
                )
        ?>

        <?= 
            $form   
                    ->field($model,'meses')
                    ->checkboxList(
                        $model->listadoMeses
                    )
        ?>                       
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario -->
