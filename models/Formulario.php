<?php
namespace app\models;
use yii\base\Model;

class Formulario extends Model {
// todos los campos del formulario
    public ?string $nombre=null;
    public  ?string $apellidos=null;
    public ?string $fechaNacimiento=null;
    public ?string $correo=null;
    public ?string $poblacion=null;
    public array $meses=[];

    public function attributeLabels():array{
        return [];
    }

    public function rules():array{
        return [
           [['nombre','apellidos','fechaNacimiento','correo','poblacion','meses'],'required'],
           [['fechaNacimiento'],'date','format'=>'yyyy-MM-dd','max'=>'2023-12-31'], 
        ];
    }

    /**
     * Son las poblaciones a mostrar en el desplegable
     */
    public function getPoblaciones(){
        return [
            "Laredo"=>"Laredo",
            "Santander"=>"Santander",
            "Astillero"=>"Astillero"
        ];
    }
    /**
     * Son los meses a mostrar en el control de checkboxlist
     */
    public function getListadoMeses() {
        return [
                "junio"=>"junio",
                "julio"=>"julio",
                "agosto"=>"agosto",
                "septiembre"=>"septiembre"
            ];
    }

    public function getMesesSeleccionados(){
        return join(",",$this->meses);
    }

}